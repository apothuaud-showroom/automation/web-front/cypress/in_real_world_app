/// <reference types="Cypress" />

describe('login', () => {

    beforeEach(() => {
        // signin first
        cy.login()
        cy.visit('/settings')
    })

    it('greets with Your Settings', () => {
        cy.contains('h1', 'Your Settings')
    })
})