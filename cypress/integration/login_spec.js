/// <reference types="Cypress" />

describe('login', () => {

    beforeEach(() => {
        cy.visit('/login')
    })

    it('greets with signin', () => {
        cy.contains('h1', 'Sign In')
    })

    it('links to register', () => {
        cy.contains('Need an account?').should('have.attr', 'href', '/register')
    })

    it('requires email', () => {
        cy.get('form').contains('Sign in').click()
        cy.get('.error-messages').should('contain', 'email or password is invalid')
    })

    it('requires password', () => {
        cy.get('[type=email]').type('joedalton@example.com{enter}')
        cy.get('.error-messages').should('contain', 'email or password is invalid')
    })

    it('requires valid username and password', () => {
        cy.get('[type=email]').type('joedalton@example.com')
        cy.get('[type=password]').type('joedalton{enter}')
        cy.get('.error-messages').should('contain', 'email or password is invalid')
    })

    it('redirects to / on success', () => {
        cy.get('[type=email]').type('joedalton@example.com')
        cy.get('[type=password]').type('joedalton123{enter}')
        cy.hash().should('eq', '')
    })
})